document.addEventListener('DOMContentLoaded', () => {
    const movieInfoContainer = document.getElementById('movieInfo');

    function searchMovie() {
        const apiKey = '30063268';
        const movieTitle = document.getElementById('movieTitle').value.trim();

        if (movieTitle.length < 3) {
            alert('Por favor, ingresa un título de película valido.');
            return;
        }

        fetch(`https://www.omdbapi.com/?t=${encodeURIComponent(movieTitle)}&plot=full&apikey=${apiKey}`)
            .then(response => response.json())
            .then(data => {
                if (data.Error || data.Title.toLowerCase() !== movieTitle.toLowerCase()) {
                    alert('Película no encontrada. Inténtalo de nuevo con otro título.');
                } else {
                    displayMovieInfo(data);
                }
            })
            .catch(error => {
                console.error('Error al consumir la API:', error);
                alert('Hubo un error al obtener la información de la película. Inténtalo de nuevo.');
            });
    }

    function displayMovieInfo(movie) {
        const titleElement = document.getElementById('title');
        const yearElement = document.getElementById('year');
        const actorsElement = document.getElementById('actors');
        const plotElement = document.getElementById('plot');
        const posterElement = document.getElementById('poster');

        titleElement.textContent = movie.Title;
        yearElement.textContent = movie.Year;
        actorsElement.textContent = movie.Actors;
        plotElement.textContent = movie.Plot;
        posterElement.src = movie.Poster;

        movieInfoContainer.classList.remove('hidden');
    }

    document.getElementById('movieTitle').addEventListener('input', () => {
        movieInfoContainer.classList.add('hidden');
    });

    document.getElementById('movieTitle').addEventListener('change', searchMovie);
});
